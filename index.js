//backend

// a)
// application
// setup port

//b)

import express, { json } from "express";
import firstRouter from "./src/routes/firstRouter.js";
import schoolRouter from "./src/routes/schoolRouter.js";
import vehicleRouter from "./src/routes/vehilceRouter.js";
import mongoose from "mongoose";
import connectToMongoDb from "./src/connectMongoDb.js";
import teacherRouter from "./src/routes/teacherRoutes.js";

let expressApp = express();
expressApp.use(json());
let port = 8000;

// expressApp.use((req, res, next) => {
//   console.log("A");

//   res.json("abc");

//   next();
// });

// expressApp.use((err, req, res, next) => {
//   console.log("B");
//   next();
// });

expressApp.use("/a", firstRouter);
expressApp.use("/schools", schoolRouter); //localhost:8000/schools
expressApp.use("/vehicles", vehicleRouter); //localhost:8000/schools
expressApp.use("/teachers", teacherRouter);
expressApp.listen(port, () => {
  console.log(`application is connected at port ${port} successfully.`);
});

expressApp.use((req, res, next) => {
  console.log("C");
});

connectToMongoDb();

//defeine Router

// use that Router at index.js

//
// body => req.body
// query => req.query

//database
// mongodb database

//connect mongodb database with express
//mongoose
