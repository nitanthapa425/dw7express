import { model } from "mongoose";
import detailSchema from "./detailsSchema.js";
import teacherSchema from "./teacherSchema.js";

export let Detail = model("Detail", detailSchema);
export let Teacher = model("Teacher", teacherSchema);

//
//=>
//=>
//=>
//=>
//=>

//
// create => post
// read => get
// updated => patch
// delete  =>  delete

// create
// send data from postman
// Detail.create(req.body) =>

// read-all
// Detail.find({}) =>

// read-specific
// Detail.findById(id)

//updated
// Detail.findByIdAndUpdate(id,req.body)

//delete
// Detail.findByIdAndDelete(id)

//localhost:8000/teachers
// method  post

//localhost:8000/teachers
// method get
