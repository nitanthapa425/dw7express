import { Router } from "express";

let firstRouter = Router();

firstRouter
  .route("/") //localhost:8000/a/
  .post((req, res) => {
    res.json("home post");
  })
  .get((req, res) => {
    res.json("home get");
  })
  .patch((req, res) => {
    res.json("hom patch");
  })
  .delete((req, res) => {
    res.json("home delete");
  });

firstRouter
  .route("/name")
  .post((req, res) => {
    res.json("name post");
  })
  .get((req, res) => {
    res.json("name get");
  })
  .patch((req, res) => {
    res.json("name patch");
  })
  .delete((req, res) => {
    res.json("name delete");
  });

export default firstRouter;

// url=localhost:8000,post at response "home post"
// 			url=localhost:8000,get at response "home get"
// 			url=localhost:8000,patch at response "home patch"
// 			url=localhost:8000,delete at response "home delete"

// url=localhost:8000/name,post at response "name post"
// 			url=localhost:8000/name,get at response "name get"
// 			url=localhost:8000/name,patch at response "name patch"
// 			url=localhost:8000/name,delete at response "name delete"
