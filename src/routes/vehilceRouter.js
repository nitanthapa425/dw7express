import { Router } from "express";

let vehicleRouter = Router();

vehicleRouter
  .route("/") //localhost:8000/vehicles
  .post((req, res) => {
    res.json({
      success: true,
      message: "vehicle created successfully.",
      data: req.body,
      query: req.query,
    });
  })
  .get((req, res) => {
    res.json({
      success: true,
      message: "vehicle read successfully.",
    });
  })
  .patch((req, res) => {
    res.json({
      success: true,
      message: "vehicle updated successfully.",
    });
  })
  .delete((req, res) => {
    res.json({
      success: true,
      message: "vehicle deleted successfully.",
    });
  });

export default vehicleRouter;
