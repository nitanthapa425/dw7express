import { Router } from "express";
import { Teacher } from "../schema/mode.js";

let teacherRouter = Router();

teacherRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;

    try {
      let result = await Teacher.create(data);
      res.json({
        success: true,
        message: "teacher created successfully.",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get((req, res) => {
    res.json({
      success: true,
      message: "teacher read successfully.",
    });
  });

export default teacherRouter;

// Teacher.create(req.body)
// Teacher.find({})
// Teacher.findById(id)
// Teacher.findByIdAndUpdate(id,req.body)
// Teacher.findByIdAndDelete(id)
