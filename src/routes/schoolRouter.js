import { Router } from "express";

let schoolRouter = Router();

schoolRouter
  .route("/") //localhost:8000/schools
  .post(
    (req, res, next) => {
      console.log("1");
      next("hari");
    },
    (req, res, next) => {
      console.log("2");
      next("ram");
    },
    (err, req, res, next) => {
      console.log("error");
    },
    (req, res, next) => {
      console.log("3");
    }
  )
  .get((req, res, next) => {
    res.json({
      success: true,
      message: "school read successfully.",
    });
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "school updated successfully.",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "school deleted successfully.",
    });
  });

schoolRouter
  .route("/details") //localhost:8000/schools/details
  .post((req, res) => {
    res.json("hello");
  });

schoolRouter
  .route("/:id1") //localhost:8000/schools/any
  .post((req, res) => {
    console.log(req.params);
    /**
     *
     * {
     * id1:12423
     *
     * }
     */
    res.json({
      success: true,
      message: "askldfjasklf",
      data: req.params,
    });
  });

export default schoolRouter;

//middleware
//it is a function which has req, res, next

//controller
// it is a middleware used at last

//middleware is divided into two part based on error
// normal middleware

// define   (req,res,next)=>{}
// to trigger  normal middleware  next()

// error middleware

// define  (err,req,res,next)=>{}
// to trigger  error middleware  next(value)

/* 
middleware is divided into two parts based on its location
route level middleware
application middleware

 */
